#pragma once

#include <string>

using std::string;

namespace character {

	enum attributeName
	{
		STRENGTH, DEXTERITY, CONSTITUTION,
		CHARISMA, MANIPULATION, APPEARANCE,
		INTELLIGENCE, PERCEPTION, WITS
	};
}
